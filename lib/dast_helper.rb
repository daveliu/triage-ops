# frozen_string_literal: true

require_relative 'team_member_select_helper'

module DastHelper
  include TeamMemberSelectHelper

  BACKEND_TEAM_FILTER = "Secure:Dynamic Analysis BE Team"
  SPECIALTY_FILTER = "Dynamic" 
  ROLE_FILTER = "Backend Engineer,"

  def dast_be
    @dast_be ||= select_random_team_member(BACKEND_TEAM_FILTER, SPECIALTY_FILTER, ROLE_FILTER)
  end
end
