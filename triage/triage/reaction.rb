# frozen_string_literal: true

module Triage
  module Reaction
    def add_comment(body)
      path = "#{event.noteable_path}/notes"

      post_request(path, body)
    end

    def add_discussion(body)
      path = "#{event.noteable_path}/discussions"

      post_request(path, body)
    end

    private

    def post_request(path, body)
      Triage.api_client.post(path, body: { body: body }) unless Triage.dry_run?

      "POST #{PRODUCTION_API_ENDPOINT}#{path}, body: `#{body}`"
    end
  end
end
