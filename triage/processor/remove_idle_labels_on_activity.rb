# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class RemoveIdleLabelOnActivity < Processor
    COMMUNITY_CONTRIBUTION_LABEL = 'Community contribution'.freeze
    IDLE_LABEL = 'idle'
    STALE_LABEL = 'stale'

    react_to 'merge_request.note', 'merge_request.update'

    def applicable?
      event.from_gitlab_org? &&
        event.label_names.include?(COMMUNITY_CONTRIBUTION_LABEL) &&
        (event.label_names & [IDLE_LABEL, STALE_LABEL]).any?
    end

    def process
      add_comment(
        <<~COMMENT.chomp
          /unlabel ~"#{IDLE_LABEL}" ~"#{STALE_LABEL}"
        COMMENT
      )
    end
  end
end
