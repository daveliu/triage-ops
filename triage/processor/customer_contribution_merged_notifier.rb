# frozen_string_literal: true

require_relative './customer_contribution_notifier'

module Triage
  class CustomerContributionMergedNotifier < CustomerContributionNotifier
    react_to 'merge_request.merge'

    def customer_contribution_message_template
      msg = <<~MESSAGE
      > Customer MR Merged -
      MESSAGE

      msg + super
    end
  end
end
