# frozen_string_literal: true

require_relative '../lib/group_triage_helper'

Gitlab::Triage::Resource::Context.include GroupTriageHelperContext
