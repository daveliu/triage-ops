# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/infradev_label_transition'
require_relative '../../triage/triage/event'

RSpec.describe Triage::InfradevLabelTransition do
  include_context 'with event' do
    let(:event_attrs) do
      {
        from_gitlab_org?: from_gitlab_org,
        removed_label_names: removed_label_names
      }
    end
    let(:from_gitlab_org) { true }
    let(:label_names) { ['infradev'] }
    let(:removed_label_names) { [] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["issue.open", "issue.update"]

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when infradev label is present' do
      context 'when engineering allocation labels are set' do
        let(:label_names) { ['infradev', *described_class::ENGINEERING_ALLOCATION_LABELS] }

        include_examples 'event is not applicable'
      end

      context 'when not all engineering allocation labels are set' do
        let(:label_names) { ['infradev', described_class::ENGINEERING_ALLOCATION_LABELS[0]] }

        include_examples 'event is applicable'
      end
    end

    context 'when infradev label is not present' do
      let(:label_names) { [] }

      context 'when no engineering allocation labels are set' do
        include_examples 'event is not applicable'
      end

      context 'when engineering allocation labels are set' do
        let(:label_names) { [*described_class::ENGINEERING_ALLOCATION_LABELS] }

        include_examples 'event is not applicable'
      end

      context 'when not all engineering allocation labels are set' do
        let(:label_names) { [described_class::ENGINEERING_ALLOCATION_LABELS[0]] }

        include_examples 'event is not applicable'
      end
    end

    context 'when infradev label was just removed' do
      let(:removed_label_names) { ['infradev'] }

      context 'when no engineering allocation labels are set' do
        let(:label_names) { [] }

        include_examples 'event is not applicable'
      end

      context 'when engineering allocation labels are set' do
        let(:label_names) { [*described_class::ENGINEERING_ALLOCATION_LABELS] }

        include_examples 'event is applicable'
      end

      context 'when not all engineering allocation labels are set' do
        let(:label_names) { [described_class::ENGINEERING_ALLOCATION_LABELS[0]] }

        include_examples 'event is applicable'
      end
    end
  end

  describe '#process' do
    let(:labels_as_markdown) { described_class::ENGINEERING_ALLOCATION_LABELS.map { |label| %(~"#{label}") }.join(' ') }

    shared_examples 'posts a quick action' do |action|
      it do
        body = <<~MARKDOWN.chomp
          /#{action} #{labels_as_markdown}
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when infradev label is present' do
      include_examples 'posts a quick action', 'label'
    end

    context 'when infradev label was just removed' do
      let(:removed_label_names) { ['infradev'] }

      include_examples 'posts a quick action', 'unlabel'
    end
  end
end
